#!/bin/bash -xe
# Update with optional user data that will run on instance start.
# Learn more about user-data: https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/user-data.html
#mkdir -p /home/ec2-user/test
#installing libs
yum update -y
amazon-linux-extras install docker
service docker start
systemctl enable docker
usermod -a -G docker ec2-user
docker info
cd /home/ec2-user/

echo `pwd` >> /home/ec2-user/pwd.txt  

mkdir -p /root/.aws
cd ~/.aws

tee -a config <<EOF 
[default]
output = json
region = us-east-1
EOF

aws ecr get-login-password | docker login --username AWS --password-stdin 459602490943.dkr.ecr.us-east-1.amazonaws.com

docker pull 459602490943.dkr.ecr.us-east-1.amazonaws.com/hello-repository


#PUSHING IT TO OTHER ACCOUNT

docker tag 459602490943.dkr.ecr.us-east-1.amazonaws.com/hello-repository:latest 602011150591.dkr.ecr.us-east-1.amazonaws.com/hello-repository:0.0.1

aws ecr get-login-password | docker login --username AWS --password-stdin 602011150591.dkr.ecr.us-east-1.amazonaws.com

#aws ecr create-repository --repository-name hello-repository --region us-east-1

docker push 602011150591.dkr.ecr.us-east-1.amazonaws.com/hello-repository:0.0.1
